import landscapes as ls
import numpy as np
from math import inf

def test_linear_integral():
    assert ls._linear_integral(0,0,0,0,0) == 0
    assert ls._linear_integral(0,0,0,0,1) == 0
    assert ls._linear_integral(0,0,0,0,100) == 0

    assert ls._linear_integral(1,0,0,1,1) == 0.5
    assert ls._linear_integral(1,1,0,1,1) == 1.5
    assert ls._linear_integral(2,0,0,1,1) == 1.0

    assert ls._linear_integral(1,0,0,1,2) == 1/3

def test_linear_interpolate():

    assert ls._linear_interpolate(0,1,0,0,0) == 0
    assert ls._linear_interpolate(0,1,0,0,1) == 0
    assert ls._linear_interpolate(0,1,0,0,0.5) == 0
    assert ls._linear_interpolate(0,1,0,1,0) == 0
    assert ls._linear_interpolate(0,1,0,1,1) == 1
    assert ls._linear_interpolate(0,1,0,1,0.5) == 0.5

def test_X():

    X1 = [-inf, 0.0, 1.0, 2.0, inf]
    Y1 = [0,0,1,0,0]
    landscape = ls.PersistenceLandscape([X1], [Y1])

    np.testing.assert_array_equal(landscape.critical_numbers(1), np.array(X1))
    np.testing.assert_array_equal(landscape.critical_numbers(2), np.array([-inf, inf]))

def test_Y():

    X1 = [-inf, 0.0, 1.0, 2.0, inf]
    Y1 = [0,0,1,0,0]
    landscape = ls.PersistenceLandscape([X1], [Y1])

    np.testing.assert_array_equal(landscape.critical_values(1), np.array(Y1))
    np.testing.assert_array_equal(landscape.critical_values(2), np.array([0,0]))

def test_max_k():
    X = [-inf, 0.0, 1.0, 2.0, inf]
    Y = [0,0,1,0,0]

    landscape=ls.PersistenceLandscape([X], [Y])
    assert landscape.max_k() >= 1

    landscape=ls.PersistenceLandscape([X,X], [Y,Y])
    assert landscape.max_k() >= 2

def test_lambda_k():
    X = [-inf, 0.0, 1.0, 2.0, inf]
    Y = [0,0,1,0,0]

    landscape=ls.PersistenceLandscape([X]*5, [Y]*5)

    for i in range(1,6):
        assert landscape.lambda_k(i,0) == 0
        assert landscape.lambda_k(i,1) == 1
        assert landscape.lambda_k(i,2) == 0
        assert landscape.lambda_k(i,0.5) == 0.5
        assert landscape.lambda_k(i,1.5) == 0.5

    assert landscape.lambda_k(6, 1) == 0

def test_generate_landscape():
    landscape = ls.generate_landscape([[1,5], [2,8], [3,4], [5,9], [6,7]])

    assert landscape.max_k() >= 3

    lambda1 = [(1,0), (2,1), (3,2), (3.5,1.5), (5,3), (6,2), (6,2), (8,1), (9,0)] 
    lambda2 = [(2,0), (3,1), (3.5,1.5), (4,1), (5,0), (6.5,1.5), (8,0)]
    lambda3 = [(3,0), (3.5,0.5), (4,0), (5,0), (6,0), (6.5,0.5), (7,0)]

    for x,y in lambda1:
        assert landscape.lambda_k(1, x) == y
    for x,y in lambda2:
        assert landscape.lambda_k(2, x) == y
    for x,y in lambda3:
        assert landscape.lambda_k(3, x) == y 

def test_empty_barcode():
    zero_ls = ls.generate_landscape([])

    for k in range(1,10):
        for t in range(10):
            assert zero_ls.lambda_k(k,t) == 0

def test_distance_empty_barcode():
    landscape = ls.generate_landscape([[1,5], [2,8], [3,4], [5,9], [6,7]])
    zero_ls = ls.generate_landscape([])

    assert ls.distance(landscape, zero_ls, 1) == landscape.norm(1)