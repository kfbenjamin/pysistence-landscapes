"""Generate and manipulate persistence landscapes."""

from math import inf
import bisect
from typing import Union
import numpy as np
import numpy.typing as npt


def _linear_integral(a, b, lower_limit, upper_limit, p):
    """Computes the integral of |ax+b|^p from lower_limit to upper_limit.

    Assumes that ax+b doesn't change sign in this region.
    """
    if a == 0:
        return abs((b ** p) * (upper_limit - lower_limit))
    y_0 = a * lower_limit + b
    y_1 = a * upper_limit + b
    return abs((y_1**(p+1) - y_0**(p+1))/(a*(p+1)))


def _linear_interpolate(x_0, x_1, y_0, y_1, x):
    """The value of f(x), where x0<=x<=x1, if f is linear with f(x_i) = y_i."""
    return y_0 + (x - x_0)*(y_1 - y_0)/(x_1 - x_0)


class PersistenceLandscape:
    """A persistence landscape.

    Attributes:
        critical_numbers:
            A list of arrays containing the critical numbers X_k for each k.
        critical_values:
            A list of arrays containing the critical values Y_k for each k.
    """

    def __init__(self,
                 critical_numbers: Union[list[list[int]], list[npt.NDArray]],
                 critical_values: Union[list[list[int]], list[npt.NDArray]]
                 ):
        """Inits PersistenceLandscape with critical numbers and values."""
        self._critical_numbers = list(map(np.array, critical_numbers))
        self._critical_values = list(map(np.array, critical_values))

    def critical_numbers(self, k: int) -> npt.NDArray:
        """Returns the critical numbers X_k for any k>0."""
        if k > self.max_k():
            return np.array([-inf, inf])
        return self._critical_numbers[k-1]

    def critical_values(self, k: int) -> npt.NDArray:
        """Returns the critical values Y_k for any k>0."""
        if k > self.max_k():
            return np.array([0, 0])
        return self._critical_values[k-1]

    def max_k(self) -> int:
        """Returns a int K such that lambda_k is zero for all k >= K"""
        return len(self._critical_numbers)

    def lambda_k_vect(self, k: int, v_array: npt.NDArray) -> npt.NDArray:
        """Vectorised computation of lambda_k.

        Args:
            k: The level of persistence to compute.
            v: A 1d numpy array of input floats.

        Returns:
            A numpy array containing the values lambda_k(x) for each x in v.
        """
        critical_numbers_k = self.critical_numbers(k)
        critical_values_k = self.critical_values(k)
        indices = np.searchsorted(critical_numbers_k, v_array, side='right') - 1
        x_array_0, x_array_1, y_array_0, y_array_1 = np.zeros((4, len(v_array)))
        for j in range(len(v_array)):
            i = indices[j]
            if i == 0 or i >= len(critical_numbers_k) - 2:
                x_array_1[j] = 1
            else:
                x_array_0[j] = critical_numbers_k[i]
                x_array_1[j] = critical_numbers_k[i+1]
                y_array_0[j] = critical_values_k[i]
                y_array_1[j] = critical_values_k[i+1]
        output = _linear_interpolate(x_array_0, x_array_1,
                                     y_array_0, y_array_1,
                                     v_array)
        output[0] = output[-1] = 0
        return output

    def lambda_k(self, k: int, v: float) -> float:
        """Compute lambda_k(x)."""
        if k <= 0:
            raise ValueError("Cannot evaluate non-positive landscape layer")
        c_nums_k = self.critical_numbers(k)
        c_vals_k = self.critical_values(k)
        i = bisect.bisect(c_nums_k, v) - 1
        if i == 0 or i >= len(c_nums_k) - 2:
            return 0
        return _linear_interpolate(c_nums_k[i], c_nums_k[i+1],
                                   c_vals_k[i], c_vals_k[i+1],
                                   v)

    def integral(self, k: int, p: float) -> float:
        """The integral |lambda_k|^p from -inf to inf."""
        if k <= 0:
            raise ValueError("Cannot evaluate non-positive landscape layer")
        X = self.critical_numbers(k)
        Y = self.critical_values(k)
        integrals = np.empty(len(X) - 3)
        for i in range(1, len(X) - 2):
            if X[i] == X[i+1]:
                integrals[i-1] = 0
            else:
                a = (Y[i+1] - Y[i])/(X[i+1] - X[i])
                b = Y[i] - a * X[i]
                # in this region we have y = ax + b
                if Y[i] * Y[i+1] < 0:
                    # we need to split into two integrals:
                    m = -b / a
                    integrals[i-1] = (_linear_integral(a, b, X[i], m, p)
                                      + _linear_integral(a, b, m, X[i+1], p))
                else:
                    integrals[i-1] = _linear_integral(a, b, X[i], X[i+1], p)
        return integrals.sum()

    def norm(self, p: int) -> float:
        """The Lp norm of the landscape."""
        if p == inf:
            return np.max(np.abs(self.critical_values(1)))
        max_k = self.max_k()
        integrals = np.empty(max_k)
        for k in range(max_k):
            integrals[k] = self.integral(k+1, p)
        return (integrals.sum())**(1/p)

    def save(self, fname):
        """Save the landscape to storage."""
        with open(fname, 'w', encoding="utf-8") as file:
            k = 1
            for k in range(1, self.max_k() + 1):
                file.write(f"#lambda_{k}\n")
                for x, y in zip(self.critical_numbers(k), self.critical_values(k)):
                    file.write(f"{x} {y}\n")

    def plot(self, *args, **kwargs):
        raise NameError("Use landscapes.plotting.plot")


def _process_layer(diagram: list[tuple[int, int]]) -> tuple[list[int], list[int]]:

    birth, death = diagram.pop(0)

    p = 0

    critical_numbers = [-inf, birth, (birth+death)/2]
    critical_values = [0, 0, (death-birth)/2]

    while critical_numbers[-1] != inf:
        i = p
        while i < len(diagram) and diagram[i][1] <= death:
            i += 1
        if i == len(diagram):
            critical_numbers += [death, inf]
            critical_values += [0, 0]
        else:
            birth1, death1 = diagram[i]
            del diagram[i]
            p = i
            if birth1 > death:
                critical_numbers.append(death)
                critical_values.append(0)
            if birth1 >= death:
                critical_numbers.append(birth1)
                critical_values.append(0)
            else:
                critical_numbers.append((birth1+death)/2)
                critical_values.append((death-birth1)/2)
                i = p
                while i < len(diagram) and diagram[i][0] < birth1:
                    i += 1
                if i == len(diagram):
                    diagram.append((birth1, death))
                    p = len(diagram)
                elif diagram[i][0] > birth1:
                    diagram.insert(i, (birth1, death))
                    p = i+1
                else:
                    while i < len(diagram) and diagram[i][1] > death:
                        i += 1
                    if i == len(diagram):
                        diagram.append((birth1, death))
                        p = len(diagram)
                    else:
                        diagram.insert(i, (birth1, death))
                        p = i+1
            critical_numbers.append((birth1+death1)/2)
            critical_values.append((death1-birth1)/2)
            birth, death = birth1, death1

    return critical_numbers, critical_values


def generate_landscape(diagram: list[tuple[int, int]]) -> PersistenceLandscape:
    """Generate a landscape from a persistence diagram/barcode.

    Implements Algorithm 1 from [BP17].

    Args:
        diagram: a 2d numpy array of birth/death pairs.

    Returns:
        The persistence landscape of the diagram.
    """

    critical_numbers = []
    critical_values = []

    diagram = diagram.copy()
    diagram.sort(key=lambda p: (p[0], -p[1]))

    while diagram:

        c_nums_layer, c_vals_layer = _process_layer(diagram)

        critical_numbers.append(c_nums_layer)
        critical_values.append(c_vals_layer)

    return PersistenceLandscape(critical_numbers, critical_values)


def load(fname) -> PersistenceLandscape:
    """Load a persistence landscape from storage."""
    critical_numbers = []
    critical_values = []
    with open(fname, 'r', encoding="utf-8") as file:
        k = 0
        c_nums_k: list[float] = [-inf]
        c_vals_k: list[float] = [0.0]
        for line in file.readlines():
            if line[0] == '#':
                if k > 0:
                    c_nums_k.append(inf)
                    c_vals_k.append(0)
                    critical_numbers.append(c_nums_k)
                    critical_values.append(c_vals_k)
                c_nums_k = [-inf]
                c_vals_k = [0]
                k += 1
            else:
                num, val = map(float, line.split())
                c_nums_k.append(num)
                c_vals_k.append(val)
    return PersistenceLandscape(critical_numbers, critical_values)


def linear_combination(landscapes: list[PersistenceLandscape],
                       weights: list[float],
                       max_k=None
                       ) -> PersistenceLandscape:
    """Computes a linear combination of persistence landscapes.

    Implements Algorithm 3 from [BP17].

    Args:
        landscapes:
            An iterable of persistence landscapes.
        weights:
            An iterable of weights of the same length as landscapes.
        max_k:
            Optional: The largest value of k for which we consider lambda_k.
            If max_k is None then all the lambda_k's are used.

    Returns:
        The linear combination of the landscapes with the given weights.
    """
    if max_k is None:
        max_k = max(landscape.max_k() for landscape in landscapes)
    critical_numbers = []
    critical_values = []

    for k in range(1, max_k+1):
        all_critical_numbers = [landscape.critical_numbers(k)
                                for landscape in landscapes]
        critical_numbers_k = np.array(sorted({x for X in all_critical_numbers for x in X}))
        Z = np.empty((len(landscapes), len(critical_numbers_k)))
        with np.errstate(invalid='ignore'):
            for i, landscape in enumerate(landscapes):
                Z[i] = landscape.lambda_k_vect(k, critical_numbers_k) * weights[i]
        critical_values_k = np.sum(Z, axis=0)
        critical_numbers.append(critical_numbers_k)
        critical_values.append(critical_values_k)

    return PersistenceLandscape(critical_numbers, critical_values)


def average(landscapes: list[PersistenceLandscape],
            max_k=None
            ) -> PersistenceLandscape:
    """Computes an average of persistence landscapes.

    Args:
        landscapes:
            An iterable of persistence landscapes.
        max_k:
            Optional: The largest value of k for which we consider lambda_k.
            If max_k is None then all the lambda_k's are used.

    Returns:
        The average persistence landscape.
    """
    num_landscapes = len(landscapes)
    if num_landscapes == 1:
        landscape = landscapes[0]
        return PersistenceLandscape(landscape._critical_numbers[:max_k],
                                    landscape._critical_values[:max_k])
    return linear_combination(landscapes, [1/num_landscapes] * num_landscapes, max_k)


def distance(landscape1: PersistenceLandscape,
             landscape2: PersistenceLandscape,
             p: int,
             max_k=None
             ) -> float:
    """Computes the Lp distance between two landscapes."""
    if not max_k and p == inf:
        max_k = 1
    diff = linear_combination([landscape1, landscape2], [1, -1], max_k=max_k)
    return diff.norm(p)


def weighted_integral(landscape: PersistenceLandscape,
                      weights: list[float],
                      p: int = 1
                      ) -> float:
    """Computes a linear combination of the Lp norms of each lambda_k."""
    x = np.zeros(len(weights))
    for k, w in enumerate(weights):
        if w != 0:
            x[k] = w * landscape.integral(k+1, p)
    return x.sum()
