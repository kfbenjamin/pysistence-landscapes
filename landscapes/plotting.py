try:
    import matplotlib.pyplot as plt
except ModuleNotFoundError as e:
    raise ModuleNotFoundError("Plotting requires optional dependencies") from e


def plot(landscape, show_zero=False):
    """Plot the persistence landscape.
    Args:
        landscape:
            a persistence landscape
        show_zero:
            Optional: If True, then the zero values of the landscape
            are plotted. Must be set to True if the landscape takes
            negative values, or bad things will happen.
    Returns:
        A matplotlib figure containing the landscape plot.
    """
    fig, ax = plt.subplots(1)
    left_bound = landscape.critical_numbers(1)[0]
    right_bound = landscape.critical_numbers(1)[-1]
    for k in range(1, landscape.max_k() + 1):
        X_k = list(landscape.critical_numbers(k))
        Y_k = list(landscape.critical_values(k))
        if show_zero:
            X_k[0] = left_bound
            X_k[-1] = right_bound
        else:
            # Very ugly. We push regions under the axis if we don't want to
            # show them. The obvious alternative of just doing two plots
            # would require us to manually set colours, which is a lot of
            # extra work.
            i = 2
            while i < len(Y_k) - 2:
                if Y_k[i] == 0 and Y_k[i+1] == 0:
                    X_k.insert(i+1, X_k[i+1])
                    Y_k.insert(i+1, -1)
                    X_k.insert(i+1, X_k[i])
                    Y_k.insert(i+1, -1)
                    i += 2
                i += 1
        ax.plot(X_k, Y_k)
    if not show_zero:
        ax.set_ylim(bottom=0)
    return fig, ax
