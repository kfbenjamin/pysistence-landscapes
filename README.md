Python implementation of persistence landscapes from topological data analysis. Persistence landscapes are due to Bubenik [1], and the algorithms used in this implementation are taken from [2].

[1] Peter Bubenik. "Statistical Topological Data Analysis using Persistence Landscapes". Journal of Machine Learning Research 16 (2015), pp. 77--102.

[2] Peter Bubenik and Pawel Dlotko. "A persistence landscapes toolbox for topological statistics". Journal of Symbolic Computation 78 (2017), pp. 91--114.

## Installation

Install directly from the repository using pip:

```pip install git+https://gitlab.com/kfbenjamin/pysistence-landscapes.git```

For development purposes, the package is maintained with Poetry. So you can
alternatively install Poetry and then run `poetry install` in the cloned repository.

## Usage

To generate a persistence landscape from a barcode:

```python
import landscapes
barcode = [[1,5], [2,8], [3,4], [5,9], [6,7]]
landscape = landscapes.generate_landscape(barcode)
```

The variable `landscape` now points to a `PersistenceLandscape` object, which can be manipulated. The values of the landscape, call it $\lambda$, can be accessed through the method `landscape.lambda_k`. For example, `landscape.lambda_k(2,3)` returns $\lambda_2(3) = 1$. Similarly, `landscape.norm(p)` evaluates the p-norm of the landscape.

To generate a matplotlib figure of the landscape, use `landscapes.plotting.plot(landscape)`.

The function `landscapes.average` takes a list of landscapes and returns their average landscape. The function `landscape.distance(first_landscape, second_landscape, p)` computes the Lp distance between two landscapes.
